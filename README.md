# employee-details-service

This Service Provides API's to perform Operations related to Employees

Here is the Postman Link :

https://www.getpostman.com/collections/c0a4eddd3d37b650eeef


STEPS:

create a project in gitlab,
Use *git clone* "link" to clone the Project,
Used spring.io to initialize the Project which generated a ZIP which will be further extarcted to get the Basic project Structure.
Used *git push* to push the Code to gitlab, commits are registered under my company account.
Defined Basic Skeleton for Project and peroformed a commit on that as well by git command
*git commit -m "Commit Message"*
Implemented `POST`,`GET`,`PUT`,`DELETE`  API calls as per the Requirement.
Not Implemented any Test cases.