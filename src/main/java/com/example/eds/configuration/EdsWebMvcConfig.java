package com.example.eds.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * This class configures Spring MVC configuration.
 */
@Configuration
public class EdsWebMvcConfig extends WebMvcConfigurerAdapter {

    /**
     * Adds CORS (Cross Origin Resource Sharing) to CORS registry
     * @param registry {@link CorsRegistry}
     * @return @link {@link Void} 
     */
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**").allowedOrigins("*")
			.allowedHeaders("Content-Type", "Accept-Encoding","apiKey","id")
			.allowedMethods("PUT", "POST", "DELETE", "PATCH", "GET", "OPTIONS")
			.exposedHeaders("Content-Type", "Accept-Encoding","apiKey","id");
	}
}
