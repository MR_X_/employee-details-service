package com.example.eds.controller.advice;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.example.eds.controller.EmployeeDetailsServiceController;
import com.example.eds.outbound.response.ApiResponse;
import com.example.eds.outbound.response.Message;
import com.example.eds.outbound.response.Status;
import com.example.eds.util.Constants;

@RestControllerAdvice(assignableTypes = {EmployeeDetailsServiceController.class})
public class EmployeeDetailsServiceControllerAdvice {

	/**
	 * Exception handler to handle {@link Exception}
	 * @param Exception
	 * @return {@link ResponseEntity<ApiResponse>}
	 */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiResponse> handleException(Exception exception) {
    	List<Message> messages = new ArrayList<>();
    	String errorDescription ="Service Exception";
    	String detailedDescription= exception.getMessage();
    	messages.add(new Message(errorDescription, Constants.EXCEPTION_CODE, detailedDescription));
    	Status errorResponseStatus = new Status(messages);
    	if(StringUtils.equals(exception.getMessage(),Constants.EMPLOYEE_NOT_FOUND_ERROR_MESSAGE) ||
    			StringUtils.equals(exception.getMessage(),Constants.EMPLOYEES_NOT_FOUND_ERROR_MESSAGE)) {
    		return new ResponseEntity<>(new ApiResponse(errorResponseStatus, null), HttpStatus.NOT_FOUND);
    	}
    	if(StringUtils.equals(exception.getMessage(),Constants.AUTHENTICATION_ERROR)){
    		return new ResponseEntity<>(new ApiResponse(errorResponseStatus, null), HttpStatus.UNAUTHORIZED);
    	}
    	else {
    		return new ResponseEntity<>(new ApiResponse(errorResponseStatus, null), HttpStatus.INTERNAL_SERVER_ERROR);
    	}
    }
}
