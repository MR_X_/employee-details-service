package com.example.eds.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.eds.inbound.request.EmployeeInfo;
import com.example.eds.outbound.response.ApiResponse;
import com.example.eds.service.IEdsService;

/**
 * This Controller exposes a request handler to process incoming Employee Details.
 *
 */
@RestController
@RequestMapping(path = "/", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
public class EmployeeDetailsServiceController {
	
	@Autowired
	private IEdsService IEdsService;
	
	/**
	 * This method performs an action of processing Employee Details.
	 * @param employeeInfo {@link EmployeeInfo}
	 * @param httpHeaders {@link HttpHeaders}
	 * @throws Exception 
	 */
	@PostMapping(path = "/v1/employee")
	public ResponseEntity<ApiResponse> processEmployeeInfo(@RequestBody final EmployeeInfo employeeInfo, @RequestHeader final HttpHeaders httpHeaders) throws Exception {
		ResponseEntity<ApiResponse> response = IEdsService.processEmployeeRequest(employeeInfo, httpHeaders);
		return response;
	}
	
	/**
	 * This method performs an action of processing Employee Details.
	 * @param httpHeaders {@link HttpHeaders}
	 * @throws Exception 
	 */
	@GetMapping(path = "/v1/employee")
	public ResponseEntity<ApiResponse> getEmployee(@RequestHeader final HttpHeaders httpHeaders) throws Exception {
		ResponseEntity<ApiResponse> response = IEdsService.getEmployee(httpHeaders,true);
		return response;
	}
	
	/**
	 * This method performs an action of processing Employee Details.
	 * @param httpHeaders {@link HttpHeaders}
	 * @throws Exception 
	 */
	@GetMapping(path = "/v1/employees")
	public ResponseEntity<ApiResponse> getEmployeeList(@RequestHeader final HttpHeaders httpHeaders) throws Exception {
		ResponseEntity<ApiResponse> response = IEdsService.getEmployee(httpHeaders,false);
		return response;
	}

	/**
	 * This method performs an action of deleting Employee Details.
	 * @param httpHeaders {@link HttpHeaders}
	 * @throws Exception 
	 */
	@DeleteMapping(path = "/v1/employee")
	public ResponseEntity<ApiResponse> deleteEmployee(@RequestHeader final HttpHeaders httpHeaders) throws Exception {
		ResponseEntity<ApiResponse> response = IEdsService.deleteEmployee(httpHeaders);
	    return response;
	}
	
	/**
	 * This method performs an action of updating Employee Details.
	 * @param httpHeaders {@link HttpHeaders}
	 * @param employeeInfo {@link EmployeeInfo}
	 * @throws Exception 
	 */
	@PutMapping(path = "/v1/employee")
	public ResponseEntity<ApiResponse> updateEmployee(@RequestBody final EmployeeInfo employeeInfo, @RequestHeader final HttpHeaders httpHeaders) throws Exception {
		ResponseEntity<ApiResponse> response = IEdsService.updateEmployee(employeeInfo, httpHeaders);
	    return response;
	}

}
