package com.example.eds.dao;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.repository.CrudRepository;

import com.example.eds.data.model.Employee;

@Configuration
public interface IEmployeeDao extends CrudRepository<Employee, Integer> {

	
}