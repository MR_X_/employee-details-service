package com.example.eds.service;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import com.example.eds.inbound.request.EmployeeInfo;
import com.example.eds.outbound.response.ApiResponse;

/**
 * This interface provides methods to process incoming ADF from TSC
 */
public interface IEdsService {

    /**
     * This Method validates, processes and persists the incoming EmployeeInfo request
     *
     * @param employeeInfo {@link EmployeeInfo}
     * @param httpHeaders {@link HttpHeaders}     
     * @throws Exception 
     * 
     * @return ResponseEntity<ApiResponse>
     */
    public ResponseEntity<ApiResponse> processEmployeeRequest(EmployeeInfo employeeInfo, HttpHeaders httpHeaders) throws Exception;

    /**
     * This Method performs an action to get Employee Details By ID or List Of all Employees
     * @param httpHeaders {@link HttpHeaders}
     * @param isGetById {@link Boolean}
     * @throws Exception 
     * 
     * @return ResponseEntity<ApiResponse>
     */
	public ResponseEntity<ApiResponse> getEmployee(HttpHeaders httpHeaders, boolean isGetById) throws Exception;

	 /**
     * This Method performs an action to delete Employee Details By ID
     * @param httpHeaders {@link HttpHeaders}
     * @throws Exception 
     * 
     * @return ResponseEntity<ApiResponse>
     */
	public ResponseEntity<ApiResponse> deleteEmployee(HttpHeaders httpHeaders) throws Exception;

	 /**
     * This Method performs an action to Update Employee Details
     * @param employeeInfo {@link EmployeeInfo}
     * @param httpHeaders {@link HttpHeaders}     
     * @throws Exception 
     * 
     * @return ResponseEntity<ApiResponse>
     */
	public ResponseEntity<ApiResponse> updateEmployee(EmployeeInfo employeeInfo, HttpHeaders httpHeaders) throws Exception;
}
