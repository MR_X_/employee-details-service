package com.example.eds.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.eds.dao.IEmployeeDao;
import com.example.eds.data.model.Employee;
import com.example.eds.inbound.request.EmployeeInfo;
import com.example.eds.outbound.response.ApiResponse;
import com.example.eds.outbound.response.Message;
import com.example.eds.outbound.response.Payload;
import com.example.eds.outbound.response.Status;
import com.example.eds.service.IEdsService;
import com.example.eds.util.Constants;

import io.micrometer.core.instrument.util.StringUtils;

@Service
public class EmployeeDetailsServiceImpl implements IEdsService{
	
	@Autowired
	private IEmployeeDao iEmployeeDao;
	
	@Value("${api.key}")
	private String apikey;

	/**
	 * This method performs an action of Persisting Employee Details.
	 * @param employeeInfo {@link EmployeeInfo}
	 * @param httpHeaders {@link HttpHeaders}
	 * @throws Exception 
	 * @return ResponseEntity<ApiResponse>
	 */
	@Override
	public ResponseEntity<ApiResponse> processEmployeeRequest(EmployeeInfo employeeInfo, HttpHeaders httpHeaders)
			throws Exception {
		Status status = null;
		ApiResponse apiResponse = null;
		List<Message> messages = new ArrayList<Message>();
		ResponseEntity<ApiResponse> response = null;
		status = new Status(messages);
		apiResponse = new ApiResponse(status, null);

		String apiKey = httpHeaders.getFirst("apiKey");
		if (null == apiKey || !apiKey.matches(apikey)) {
			throw new Exception(Constants.AUTHENTICATION_ERROR);
		}
		try {
			if (!StringUtils.isBlank(employeeInfo.getFirstName()) && !StringUtils.isBlank(employeeInfo.getLastName())) {
				Employee employee = new Employee();
				employee.setFirstName(employeeInfo.getFirstName());
				employee.setLastName(employeeInfo.getLastName());
				employee.setMiddleName(employeeInfo.getMiddleName());
				iEmployeeDao.save(employee);

				apiResponse.getStatus().getMessages()
						.add(new Message(Constants.SUCCESS_MESSAGE, Constants.SUCCESS_CODE, "Employee Created Successfully"));
				response = new ResponseEntity<>(apiResponse, HttpStatus.CREATED);
			} else {
				apiResponse.getStatus().getMessages()
						.add(new Message(Constants.ERROR_MESSAGE, Constants.ERROR_CODE, "Missing FirstName (or) LastName"));
				response = new ResponseEntity<>(apiResponse, HttpStatus.BAD_REQUEST);
			}
			return response;
		} catch (Exception exception) {
			throw new Exception(exception.getMessage());
		}
	}

	 /**
     * This Method performs an action to get Employee Details By ID or List Of all Employees
     * @param httpHeaders {@link HttpHeaders}
     * @param isGetById {@link Boolean}
     * @throws Exception 
     * 
     * @return ResponseEntity<ApiResponse>
     */
	@Override
	public ResponseEntity<ApiResponse> getEmployee(HttpHeaders httpHeaders,boolean isGetById) throws Exception {
		Status status = null;
		ApiResponse apiResponse = null;
		List<Message> messages = new ArrayList<Message>();
		ResponseEntity<ApiResponse> response = null;
		status = new Status(messages);
		apiResponse = new ApiResponse(status, null);
		
		String apiKey = httpHeaders.getFirst("apiKey");
		String id = httpHeaders.getFirst("id");
		if (null == apiKey || !apiKey.matches(apikey)) {
			throw new Exception(Constants.AUTHENTICATION_ERROR);
		}
		/**
		 * Boolean value `isGetById` drives the database call either to fetch Employee by ID or FindAll.
		 */
		try {
			if (isGetById) {
				if (StringUtils.isBlank(id)) {
					throw new Exception(Constants.MISSING_ID_ERROR_MESSAGE);
				} else {
					Optional<Employee> employeeOutBoundResponse = iEmployeeDao.findById(Integer.valueOf(id));
					if (employeeOutBoundResponse.isPresent()) {
						apiResponse.getStatus().getMessages().add(new Message(Constants.SUCCESS_MESSAGE,
								Constants.SUCCESS_CODE, "Employee Fetched Successfully"));
						Payload<Employee> payload = new Payload<>();
						payload.setEmployee(employeeOutBoundResponse.get());
						apiResponse = new ApiResponse(status, payload);
						response = new ResponseEntity<>(apiResponse, HttpStatus.OK);
					} else {
						throw new Exception(Constants.EMPLOYEE_NOT_FOUND_ERROR_MESSAGE);
					}
				}
			}
			else {
				List<Employee> employeeOutBoundResponseList = new ArrayList<Employee>();
				iEmployeeDao.findAll().forEach(employee -> employeeOutBoundResponseList.add(employee));
				if (!employeeOutBoundResponseList.isEmpty()) {
					apiResponse.getStatus().getMessages()
							.add(new Message(Constants.SUCCESS_MESSAGE, Constants.SUCCESS_CODE, "Employees Fetched Successfully"));
					Payload<List<Employee>> payload = new Payload<>();
					payload.setEmployee(employeeOutBoundResponseList);
					apiResponse = new ApiResponse(status, payload);
					response = new ResponseEntity<>(apiResponse, HttpStatus.OK);
				} else {
					throw new Exception(Constants.EMPLOYEES_NOT_FOUND_ERROR_MESSAGE);
				}
			}
			return response;
		}catch (Exception exception) {
			throw new Exception(exception.getMessage());
		}
	}

	/**
	 * This method performs an action of Deleting Employee Details.
	 * @param httpHeaders {@link HttpHeaders}
	 * @throws Exception 
	 * @return ResponseEntity<ApiResponse>
	 */
	@Override
	public ResponseEntity<ApiResponse> deleteEmployee(HttpHeaders httpHeaders) throws Exception {
		Status status = null;
		ApiResponse apiResponse = null;
		List<Message> messages = new ArrayList<Message>();
		ResponseEntity<ApiResponse> response = null;
		status = new Status(messages);
		apiResponse = new ApiResponse(status, null);
		
		String apiKey = httpHeaders.getFirst("apiKey");
		String id = httpHeaders.getFirst("id");
		if (null == apiKey || !apiKey.matches(apikey)) {
			throw new Exception(Constants.AUTHENTICATION_ERROR);
		}
		try {
			if (StringUtils.isBlank(id)) {
				throw new Exception(Constants.MISSING_ID_ERROR_MESSAGE);
			}
			else {
				Optional<Employee> employeeOutBoundResponse = iEmployeeDao.findById(Integer.valueOf(id));
				if (!employeeOutBoundResponse.isPresent()) {
					throw new Exception(Constants.NO_EMPLOYEE_FOUND_WITH_GIVEN_ID_ERROR_MESSAGE);
				}else {
					iEmployeeDao.deleteById(Integer.valueOf(id));
					apiResponse.getStatus().getMessages()
					.add(new Message(Constants.SUCCESS_MESSAGE, Constants.SUCCESS_CODE, "Employee Deleted Successfully"));
					response = new ResponseEntity<>(apiResponse, HttpStatus.OK);
				}
			} 
			return response;
		} catch (Exception exception) {
			throw new Exception(exception.getMessage());
		}
	}

	/**
	 * This method performs an action of Updating Employee Details.
	 * @param employeeInfo {@link EmployeeInfo}
	 * @param httpHeaders {@link HttpHeaders}
	 * @throws Exception 
	 * @return ResponseEntity<ApiResponse>
	 */
	@Override
	public ResponseEntity<ApiResponse> updateEmployee(EmployeeInfo employeeInfo, HttpHeaders httpHeaders) throws Exception {
		Status status = null;
		ApiResponse apiResponse = null;
		List<Message> messages = new ArrayList<Message>();
		ResponseEntity<ApiResponse> response = null;
		status = new Status(messages);
		apiResponse = new ApiResponse(status, null);

		String apiKey = httpHeaders.getFirst("apiKey");
		String id = httpHeaders.getFirst("id");
		if (null == apiKey || !apiKey.matches(apikey)) {
			throw new Exception(Constants.AUTHENTICATION_ERROR);
		}
		try {
			if (StringUtils.isBlank(id)) {
				throw new Exception(Constants.MISSING_ID_ERROR_MESSAGE);
			}else {
				Optional<Employee> employeeOutBoundResponse = iEmployeeDao.findById(Integer.valueOf(id));
				if (!employeeOutBoundResponse.isPresent()) {
					throw new Exception(Constants.NO_EMPLOYEE_FOUND_WITH_GIVEN_ID_ERROR_MESSAGE);
				}else {
					if (!StringUtils.isBlank(employeeInfo.getFirstName()) && !StringUtils.isBlank(employeeInfo.getLastName())) {
						employeeOutBoundResponse.get().setFirstName(employeeInfo.getFirstName());
						employeeOutBoundResponse.get().setLastName(employeeInfo.getLastName());
						employeeOutBoundResponse.get().setMiddleName(employeeInfo.getMiddleName());
						iEmployeeDao.save(employeeOutBoundResponse.get());
						apiResponse.getStatus().getMessages()
						.add(new Message(Constants.SUCCESS_MESSAGE, Constants.SUCCESS_CODE, "Employee Updated Successfully"));
				response = new ResponseEntity<>(apiResponse, HttpStatus.CREATED);
					}
					else {
						apiResponse.getStatus().getMessages()
								.add(new Message(Constants.ERROR_MESSAGE, Constants.ERROR_CODE, "Missing FirstName (or) LastName"));
						response = new ResponseEntity<>(apiResponse, HttpStatus.BAD_REQUEST);
					}
				}
			}
			return response;
		} catch (Exception exception) {
			throw new Exception(exception.getMessage());
		}
	}
}
