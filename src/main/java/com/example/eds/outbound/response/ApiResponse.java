package com.example.eds.outbound.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * This represents the API response 
 */
public class ApiResponse {
	
	/**
	 * Default constructor
	 */
	public ApiResponse() {
		super();
	}

	/**
	 * This represents the status section of the response
	 */
	private Status status;

	/**
	 * This represents the payload section of the response
	 */
	@SuppressWarnings("rawtypes")
	@JsonInclude(Include.NON_NULL)
	private Payload payload;
	
	/**
	 * This represents the Exception thrown by the downstream pushes
	 */
	@JsonInclude(Include.NON_NULL)
	private String exceptionMessage;
	
	/**
	 * parameterized Constructor 
	 * @param status
	 * @param payload
	 */
	public ApiResponse(Status status, @SuppressWarnings("rawtypes") Payload payload)
	{
		this.status=status;
		this.payload=payload;
	}

	/**
	 * parameterized Constructor 
	 * @param exceptionMessage
	 */
	public ApiResponse(String exceptionMessage)
	{
		this.exceptionMessage=exceptionMessage;
	}
	
	/**
	 * This returns the status
	 * @return status
	 */
	public Status getStatus() {
		return status;
	}
	
	/**
	 * This sets the status
	 * @param status
	 */
	public void setStatus(Status status) {
		this.status = status;
	}
	
	/**
	 * This returns the payload
	 * @return payload
	 */
	@SuppressWarnings("rawtypes")
	public Payload getPayload() {
		return payload;
	}
	
	/**
	 * This sets the payload
	 * @param payload
	 */
	public void setPayload(@SuppressWarnings("rawtypes") Payload payload) {
		this.payload = payload;
	}
	
	/**
	 * This returns the exceptionMessage
	 * @return exceptionMessage
	 */
	public String getExceptionMessage() {
		return exceptionMessage;
	}

	/**
	 * This sets the exceptionMessage
	 * @return exceptionMessage
	 */
	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}

	
}
