package com.example.eds.outbound.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * This represents the response wrapper
 * @param <T>
 */
public class Payload<T> {

	/**
	 * This represents the API response
	 */
	@JsonInclude(Include.NON_NULL)
	private T employee;

    /**
	 * This represents the parameterized constructor
	 * @param response
	 */
	public Payload(T response)
	{
		this.employee=response;
	}


	/**
	 * Default Constructor
	 */
	public Payload(){
	}

	public T getEmployee() {
		return employee;
	}


	public void setEmployee(T employee) {
		this.employee = employee;
	}
}
