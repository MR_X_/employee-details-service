package com.example.eds.outbound.response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This represents the message that goes as part of Status
 *
 */
public class Message {

	private static final Logger logger = LoggerFactory.getLogger(Message.class);

	/**
	 * Represents Description
	 */
	private String description;

	/**
	 * Represents responseCode
	 */
	private String responseCode;

	/**
	 * Represents Detailed Description
	 */
	private String detailedDescription;

	/**
	 * Parameterized Constructor
	 * 
	 * @param description
	 * @param responseCode
	 * @param detailedDescription
	 */
	public Message(String description, String responseCode, String detailedDescription) {
		this.description = description;
		this.responseCode = responseCode;
		this.detailedDescription = detailedDescription;
	}
	
	/**
	 * Default Constructor
	 */
	public Message() {
		
	}

	/**
	 * This returns the description
	 * 
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * This sets the description
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * This returns the ResponseCode
	 * 
	 * @return responseCode
	 */
	public String getResponseCode() {
		return responseCode;
	}

	/**
	 * This sets the ResponseCode
	 * 
	 * @param responseCode
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	/**
	 * This returns the DetailedDescription
	 * 
	 * @return detailedDescription
	 */
	public String getDetailedDescription() {
		return detailedDescription;
	}

	/**
	 * This sets the detailedDescription
	 * 
	 * @param detailedDescription
	 */
	public void setDetailedDescription(String detailedDescription) {
		this.detailedDescription = detailedDescription;
	}

	@Override
	public String toString() {
		return "Message [description=" + description + ", responseCode=" + responseCode + ", detailedDescription="
				+ detailedDescription + "]";
	}
}
