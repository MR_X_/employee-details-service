package com.example.eds.util;

public class Constants {
	
	private Constants() {}

	public static final String AUTHENTICATION_ERROR = "Authorization Error";
	public static final String SUCCESS_MESSAGE = "Request Processed Successfully";
	public static final String SUCCESS_CODE = "0000";
	public static final String ERROR_MESSAGE = "Bad Request";
	public static final String ERROR_CODE = "0001";
	public static final String EMPLOYEE_NOT_FOUND_ERROR_MESSAGE = "Employee Not Found";
	public static final String EMPLOYEES_NOT_FOUND_ERROR_MESSAGE = "EmployeeS Not Found";
	public static final String MISSING_ID_ERROR_MESSAGE = "Id cannot be Null";
	public static final String EXCEPTION_CODE = "EX-100";
	public static final String NO_EMPLOYEE_FOUND_WITH_GIVEN_ID_ERROR_MESSAGE = "No Employee Found with Given ID";
}
