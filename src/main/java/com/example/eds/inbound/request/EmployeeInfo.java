package com.example.eds.inbound.request;

public class EmployeeInfo {
	
	private String firstName;
	private String lastName;
	private String middleName;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public EmployeeInfo(String firstName, String lastName, String middleName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.middleName = middleName;
	}

	@Override
	public String toString() {
		return "EmployeeInfo [firstName=" + firstName + ", lastName=" + lastName + ", middleName=" + middleName + "]";
	}

}
