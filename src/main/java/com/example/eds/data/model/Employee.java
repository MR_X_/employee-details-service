package com.example.eds.data.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Represents Employee Data Model
 *
 */
@Entity
public class Employee {
	
	@GeneratedValue
	@Id
	private int employeeID;
	private String firstName;
	private String lastName;
	private String middleName;

	public int getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(int employeeID) {
		this.employeeID = employeeID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public Employee(int employeeID, String firstName, String lastName, String middleName) {
		this.employeeID = employeeID;
		this.firstName = firstName;
		this.lastName = lastName;
		this.middleName = middleName;
	}

	public Employee() {
		super();
	}

	@Override
	public String toString() {
		return "Employee [employeeID=" + employeeID + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", middleName=" + middleName + "]";
	}
}
